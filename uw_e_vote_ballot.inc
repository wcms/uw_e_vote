<?php

/**
 * @file
 * Code to generate Food Services product search form and results.
 */

/**
 * Page callback for a ballot.
 *
 * @param string $election_id
 *   The election_id.
 *
 * @return array
 *   The render array.
 */
function uw_e_vote_ballot($election_id) {
  if (function_exists('uw_e_vote_election_config')) {
    $election = uw_e_vote_election_config();
  }
  if (empty($election[$election_id])) {
    return drupal_not_found();
  }
  $election = $election[$election_id];
  $election['id'] = $election_id;

  drupal_set_title(t('Ballot:') . ' ' . $election['name']);

  return drupal_get_form('uw_e_vote_ballot_form', $election);
}

/**
 * Form builder function for a ballot.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 * @param array $election
 *   Election configuration from uw_e_vote_elections().
 *
 * @return array
 *   The render array.
 */
function uw_e_vote_ballot_form(array $form, array &$form_state, array $election) {
  $form = array();
  $form['#action'] = 'https://ego.uwaterloo.ca/~uwdir/E-vote';
  $form['#token'] = FALSE;
  $form['#pre_render'][] = '_uw_e_vote_form_remove_token';

  $form['ballot'] = array(
    '#type' => 'hidden',
    '#value' => $election['id'],
  );
  $form['intro']['#markup'] = '<ul>
  <li>Enter your vote on the ballot below.</li>
  <li>Cast your ballot by clicking on the "Cast Ballot" button below.</li>
  <li>Provide your <a href="http://watiam.uwaterloo.ca/search/">WatIAM user ID and password</a> (the same as you use to access myHRInfo and Quest) when prompted.</li>
</ul>
<p><a href="' . base_path() . 'candidates-statements/' . $election['id'] . '"><strong>Candidates\' Statements</strong></a></p>
<p>You may vote for <strong>' . $election['number_to_be_elected'] . '</strong> of the following candidates:</p>';
  foreach ($election['candidates'] as $candidate) {
    $form['candidate-' . $candidate] = array(
      '#type' => 'checkbox',
      '#title' => filter_xss('<a href="' . base_path() . 'candidates-statements/' . $election['id'] . '/' . preg_replace('/[^a-z]+/', '-', str_replace('\'', '', strtolower($candidate))) . '">' . $candidate . '</a>', array('a')),
      '#name' => $candidate,
      '#return_value' => NULL,
    );
  }
  $form['Declined'] = array(
    '#type' => 'checkbox',
    '#title' => 'Declined',
    '#name' => 'Declined',
    '#return_value' => NULL,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Cast Ballot',
    '#name' => '',
  );
  $form['outro']['#markup'] = '<p><a href="mailto:' . $election['ro_email'] . '">Contact ' . $election['ro_name'] . '</a> if you are unable to submit your ballot.</p>';
  return $form;
}

/**
 * Helper function to remove the submission tokens form a form.
 *
 * @param array $form
 *   The form array.
 *
 * @return array
 *   The modified form array.
 */
function _uw_e_vote_form_remove_token(array $form) {
  unset($form['form_token']);
  unset($form['form_build_id']);
  unset($form['form_id']);
  return $form;
}
