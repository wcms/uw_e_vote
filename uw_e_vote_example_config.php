<?php

/**
 * @file
 * Example election configuration file.
 *
 * To create an election:
 *  1. Copy this file to a convenient place, such as the same directory as the
 *     site's settings.php file.
 *  2. Edit your copy of this file to have the candidate information, etc. for
 *     your election.
 *  3. In your settings.php file, call the PHP require_once() function with the
 *     filename of your edited file.
 *  4. Enable the uw_e_vote module.
 *
 *  Ballots will appear at "ballot/<election-id>". Check the ballot for the
 *  links to where you must put your candidates' statements.
 */

/**
 * Return information about all the valid elections.
 *
 * $return array
 *   Array with keys which are election IDs and values which are array:
 *   - name: string, name of the election.
 *   - number_to_be_elected: int, how many of the candidates will be elected.
 *   - candidates: array of string, names of the candidates.
 *   - ro_name: name of the returning officer.
 *   - ro_email: email address of the returning officer.
 */
function uw_e_vote_election_config() {
  return array(
    'election-1-id' => array(
      'name' => 'Name of Election One',
      'number_to_be_elected' => 2,
      'candidates' => array('Candidate One', 'Candidate Two', 'Candidate Three'),
      'ro_name' => 'Returning Officer Name',
      'ro_email' => 'ro_email@uwaterloo.ca',
    ),
    'election-2-id' => array(
      'name' => 'Name of Election Two',
      'number_to_be_elected' => 1,
      'candidates' => array('Candidate One', 'Candidate Two'),
      'ro_name' => 'Returning Officer Name',
      'ro_email' => 'ro_email@uwaterloo.ca',
    ),
    // Add more elections if desired. If only 1 election, remove all but that one.
  );
}
